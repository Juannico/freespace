using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHelp : MonoBehaviour
{
    private int isActive;
    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.SetInt("isActive" + this.gameObject.name,0);
        isActive = PlayerPrefs.GetInt("isActive" + this.gameObject.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive == 0) this.gameObject.SetActive(true);
        else if(isActive == 1)this.gameObject.SetActive(false);
        
    }
    public void Close() {
        PlayerPrefs.SetInt("isActive" + this.gameObject.name, 1);
        isActive = PlayerPrefs.GetInt("isActive" + this.gameObject.name);
    }
}
