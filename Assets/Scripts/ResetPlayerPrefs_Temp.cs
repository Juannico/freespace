using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPlayerPrefs_Temp : MonoBehaviour
{
    public static ResetPlayerPrefs_Temp instance;
    [SerializeField]
    private SpaceShips_ScriptableObject spaceShips;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        PlayerPrefs.DeleteAll();
        spaceShips.index =0;
        for (int i = 0; i < spaceShips.spaceships.Length; i++)
        {
            spaceShips.spaceships[i].isPlayable = false;
            spaceShips.spaceships[i].activeMaterialIndex = 0;
            if (i == 0)
            {
                spaceShips.spaceships[i].isPlayable = true;
                spaceShips.spaceships[i].activeMaterialIndex = 1;
            }
        }
        for (int i = 2; i < spaceShips.materials.Length; i++)
        {
            spaceShips.materials[i].isWearble = false;
        }
        this.gameObject.SetActive(false);

      
    }

    // Update is called once per frame

}
