using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertCollision : MonoBehaviour
{
    [SerializeField]
    public GameObject warningSign;

    private bool isColliding ;
    private float timer;
    [SerializeField]
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        warningSign.SetActive(false);
        timer = 0;
        //player = GameObject.FindGameObjectWithTag("Player");
        
    }
 
    // Update is called once per frame
    void Update()
    {
        if (isColliding)
        {

            timer += Time.deltaTime;
            if (timer > 0.2f)
            {
                int timertoInt = (int)(timer * 10);
                if (timertoInt % 9 == 0) warningSign.SetActive(false);
                else warningSign.SetActive(true);
            }
        }
        else { 
        
        }
        if (!player.GetComponent<ShipController>().isAlive) {
            warningSign.SetActive(false);
            this.GetComponent<AlertCollision>().enabled = false;
        }
    }
    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Scene")) {
            isColliding = true;
        }
    }
    private void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Scene"))
        {
            timer = 0;
            warningSign.SetActive(false);
            isColliding = false;
        }
    
}
}
