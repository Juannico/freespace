using System.Collections;
using UnityEngine;

public class CameraCollider : MonoBehaviour

{
    [SerializeField]
    private GameObject player;
    private Vector3 initialPosition;
    private float hitMaxDistance;
    public bool change;
    private float distance;
    private float prewditance;

    private void Awake()
    {
        initialPosition = transform.localPosition;
    }
    // Start is called before the first frame update
    void Start()
    {

        hitMaxDistance = 4.7f;
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(player.transform.position + (Vector3.forward *8 ) , transform.TransformDirection(Vector3.up), out hit, 100f) && hit.transform.tag == "Scene")
        {

            if (prewditance - hit.distance > 2f)
            {
                change = true;
                StartCoroutine(CameraAlign(prewditance - hit.distance));
            }
            else
            {
                if(!change) distance = hitMaxDistance - hit.distance;
            }
            prewditance = hit.distance;
        }
        if (distance <0 ) {
            distance = 0;
        }     
        transform.localPosition = new Vector3(transform.localPosition.x, initialPosition.y - (distance * 1.5f),transform.localPosition.z);
    }
    private IEnumerator CameraAlign( float diference ) {

        while (diference > 0.5f) {
            diference -= Time.deltaTime;
            distance -= Time.deltaTime;
            yield return null;
        }
        change = false;
    }
}
