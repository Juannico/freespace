using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectSide : MonoBehaviour
{
    [SerializeField]
    private GameObject allert;
    private bool iscolliding;
    // Start is called before the first frame update
    private void Start()
    {
        allert.SetActive(false);
    }
    public void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Scene"))
        {
            iscolliding = true;
            StartCoroutine(WaitFor(0.2f));
        }
    }
    public void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Scene"))
        {
            StopCoroutine(WaitFor(0.1f));
            iscolliding = false;
            allert.SetActive(false);
        }
    }
    private IEnumerator WaitFor(float time ) { 
        yield return new WaitForSeconds(time);
        if (iscolliding) allert.SetActive(true);

    }

}

