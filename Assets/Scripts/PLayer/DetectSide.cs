using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetetcCollider : MonoBehaviour
{
    [SerializeField]
    private GameObject allert;
        // Start is called before the first frame update

    public void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Scene"))
        {
            allert.SetActive(true);
        }
    }
    public void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Scene"))
        {
            allert.SetActive(false);
        }
    }

}

