
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PickSpaceship : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private GameObject buyButton, noMoney, materialsUI,materialButton,buyButtonMat,blockMaterial,selectButton,selectMatButton,moneyButton;
    [SerializeField]
    private GameObject[] selectSpaceshipButton;
    [SerializeField]
    private GameObject[] selectMaterialButton;

    [SerializeField]
    public SpaceShips_ScriptableObject spaceshipSO;

    private GameObject[] spaceshipCreated;


    [SerializeField]
    private TMP_Text nameSpaceship, priceSpaceship, nameMaterial, priceMaterial, tittle ;
    private int index,indexMaterial;
    void Start()
    {
        spaceshipCreated = new GameObject[spaceshipSO.spaceships.Length];       
        if (SceneManager.GetActiveScene().name == "SelectSpaceship") {
            for (int i = 0; i < spaceshipSO.spaceships.Length; i++)
            {
                if (i == 0) spaceshipSO.spaceships[i].isPlayable = true;
                if (spaceshipSO.spaceships[i].isPlayable) buyButton.SetActive(false);

                spaceshipCreated[i] = Instantiate(spaceshipSO.spaceships[i].spaceship, Vector3.zero, Quaternion.identity);
                SetMaterial(i, spaceshipSO.spaceships[i].activeMaterialIndex);
               
                spaceshipCreated[i].SetActive(false);
                index = 0;
                indexMaterial = 0;
            }

            index = spaceshipSO.index;
            CurrentlySpaceship();
            
        }
        if (index > 1) selectSpaceshipButton[1].SetActive(true);
        if (index < spaceshipSO.materials.Length - 1) selectSpaceshipButton [0].SetActive(true);
        if (index > 0) selectSpaceshipButton[1].SetActive(true);
        if (index < spaceshipSO.spaceships.Length - 1) selectSpaceshipButton[0].SetActive(true);
    }


    private void Update()
    {
        if (PlayerPrefs.GetInt("canReward") == 1)
        {
            moneyButton.SetActive(true);
        }
        if (PlayerPrefs.GetInt("canReward") == 0)
        {
            moneyButton.SetActive(false);
        }
        foreach (GameObject spaceShip in spaceshipCreated) {
            spaceShip.transform.Rotate(Vector3.up * 15 * Time.deltaTime);
        }

    }
    public void ChangeModel(bool next)
    {
        selectSpaceshipButton[0].SetActive(false);
        selectSpaceshipButton[1].SetActive(false);
       
        spaceshipCreated[index].SetActive(false);
        if (next )index += 1;
        if (!next) index -= 1;
        if (index > 0) selectSpaceshipButton[1].SetActive(true);
        if (index < spaceshipSO.spaceships.Length - 1) selectSpaceshipButton[0].SetActive(true);
        CurrentlySpaceship();
        
    }

    public void SetModel() {
        if (spaceshipSO.spaceships[spaceshipSO.index].isPlayable)
        {
            spaceshipSO.index = index;
            selectButton.SetActive(false);
        }
        else {
            FindObjectOfType<AudioManager>().StartCoroutine("WrongSound");
            
        }
    }

    public void BuyModel() {
        int money = FindObjectOfType<Money>().money;
        if (money >= spaceshipSO.spaceships[index].price)
        {
            FindObjectOfType<Money>().SubstactMoney(spaceshipSO.spaceships[index].price);

            buyButton.SetActive(false);
            selectButton.SetActive(true);
            materialButton.SetActive(true);
            priceSpaceship.gameObject.SetActive(false);
            spaceshipSO.spaceships[index].isPlayable = true;
            spaceshipSO.spaceships[index].activeMaterialIndex = 1;
            SetMaterial(index, spaceshipSO.spaceships[index].activeMaterialIndex);
        }
        else {
            FindObjectOfType<AudioManager>().StartCoroutine("WrongSound");
            noMoney.SetActive(true);
            StartCoroutine(WairFor(1, noMoney));
        }
    }
    private void CurrentlySpaceship() 
    {
        spaceshipCreated[index].SetActive(true);
        if (spaceshipSO.spaceships[index].isPlayable) 
        { 
            buyButton.SetActive(false);
            priceSpaceship.gameObject.SetActive(false);
            materialButton.SetActive(true);
            selectButton.SetActive(true);
        }
        else
        {
            buyButton.SetActive(true);
            priceSpaceship.gameObject.SetActive(true);
            materialButton.SetActive(false);
            selectButton.SetActive(false);
        }
        if(index == spaceshipSO.index) selectButton.SetActive(false);
        nameSpaceship.text = spaceshipSO.spaceships[index].name;
        priceSpaceship.text = "Price: $"+ spaceshipSO.spaceships[index].price.ToString();

        if (spaceshipSO.spaceships[index].price == 0) priceSpaceship.text = "Free";
    }
    public void OpenMaterialsMenu() {
        StartCoroutine(MenuMaterials(false));
        tittle.text = "Material";
        indexMaterial = spaceshipSO.spaceships[index].activeMaterialIndex;
        if (spaceshipSO.materials[indexMaterial].isWearble)
        {
            buyButtonMat.SetActive(false);
            blockMaterial.SetActive(false);
        }
        else {
            buyButtonMat.SetActive(true);
            blockMaterial.SetActive(true);
        }
        selectMaterialButton[0].SetActive(false);
        selectMaterialButton[1].SetActive(false);
        if (indexMaterial > 1) selectMaterialButton[1].SetActive(true);
        if (indexMaterial < spaceshipSO.materials.Length - 1) selectMaterialButton[0].SetActive(true);
        CurrentMaterial();
    }
    public void CloseMaterialsMenu() {
        StartCoroutine(MenuMaterials(true));
        blockMaterial.SetActive(false);
        tittle.text = "Spaceship";
        SetMaterial(index, spaceshipSO.spaceships[index].activeMaterialIndex);
    }
    public void ChangeMaterial(bool next)
    {
        selectMaterialButton[0].SetActive(false);
        selectMaterialButton[1].SetActive(false);
        if (next) indexMaterial += 1;
        if (!next) indexMaterial -= 1;
        if (indexMaterial > 1) selectMaterialButton[1].SetActive(true);
        if (indexMaterial < spaceshipSO.materials.Length - 1) selectMaterialButton[0].SetActive(true);
        SetMaterial(index,indexMaterial);
        CurrentMaterial();
    }
    private void CurrentMaterial()
    {

        if(spaceshipSO.materials[indexMaterial].isWearble)
        {
            buyButtonMat.SetActive(false);
            priceMaterial.gameObject.SetActive(false);
            blockMaterial.SetActive(false);
            selectMatButton.SetActive(true);
        }
        else
        {
            buyButtonMat.SetActive(true);
            blockMaterial.SetActive(true);
            selectMatButton.SetActive(false);
            priceMaterial.gameObject.SetActive(true);
        }
        if (indexMaterial == spaceshipSO.spaceships[index].activeMaterialIndex) selectMatButton.SetActive(false);
        nameMaterial.text = "Color: " + spaceshipSO.materials[indexMaterial].nameMaterial;
        priceMaterial.text = "Price: $" + spaceshipSO.materials[indexMaterial].price.ToString();
        if (spaceshipSO.materials[indexMaterial].price == 0) priceMaterial.text = "Free";
    }

    public void BuyMaterial()
    {
        int money = FindObjectOfType<Money>().money;
        if (money >= spaceshipSO.materials[indexMaterial].price)
        {
            FindObjectOfType<Money>().SubstactMoney(spaceshipSO.spaceships[index].price);
            buyButtonMat.SetActive(false);
            blockMaterial.SetActive(false);
            selectMatButton.SetActive(true);
            spaceshipSO.materials[indexMaterial].isWearble = true;
            spaceshipSO.spaceships[index].activeMaterialIndex = indexMaterial;
            SetMaterial(index, spaceshipSO.spaceships[index].activeMaterialIndex);
        }
        else
        {
            FindObjectOfType<AudioManager>().StartCoroutine("WrongSound");
            noMoney.SetActive(true);
            StartCoroutine(WairFor(1, noMoney));
        }
    }

    public void SelectMaterial()
    {
        if (spaceshipSO.materials[indexMaterial].isWearble)
        {
            spaceshipSO.spaceships[index].activeMaterialIndex = indexMaterial;
            selectMatButton.SetActive(false);
        }   
        else
        {
            FindObjectOfType<AudioManager>().StartCoroutine("WrongSound");

        }
    }

    private void SetMaterial(int indexSpaceship, int indexMaterialTemp)
    {
        if (!spaceshipSO.spaceships[indexSpaceship].isPlayable) spaceshipSO.spaceships[indexSpaceship].activeMaterialIndex = 0;
        if (spaceshipCreated[indexSpaceship].GetComponent<Renderer>() == null)
        {
            Renderer[] spaceshipsRenderer = spaceshipCreated[indexSpaceship].GetComponentsInChildren<Renderer>();
            foreach (Renderer renderer in spaceshipsRenderer)
            {
                renderer.material = spaceshipSO.materials[indexMaterialTemp].material;
            }

        }
        else
        {
            spaceshipCreated[indexSpaceship].GetComponent<Renderer>().material = spaceshipSO.materials[indexMaterialTemp].material;
        }
    }
    public IEnumerator WairFor(int seconds, GameObject obj) {
        yield return new WaitForSeconds(seconds);
        obj.SetActive(false);
    }

    IEnumerator MenuMaterials(bool isOpen) {
        float numeric = 2100;      
        if (isOpen) {
            while (materialsUI.transform.position.y > -366) {
                materialsUI.transform.position -= Vector3.up * numeric * Time.deltaTime;
                numeric -= 1800 * Time.deltaTime;
                yield return null;
            }
 
            materialsUI.transform.position = new Vector3(Screen.width / 2, -384, 0);
        }
        else
        {
            
            while (materialsUI.transform.position.y < 484)
            {
                materialsUI.transform.position += Vector3.up * numeric * Time.deltaTime;
                numeric -= 1800 * Time.deltaTime;
                yield return null;
            }
            while (materialsUI.transform.position.y > 484)
            {
                materialsUI.transform.position -= Vector3.up * numeric/8 * Time.deltaTime;
                yield return null;
            }
            
            materialsUI.transform.position = new Vector3(Screen.width / 2, 484, 0);
        }
    }
}
