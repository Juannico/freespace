using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyUI.PickerWheelUI;

public class ShipController : MonoBehaviour
{
    private GameObject SpaceshipModel, SpaceShipTail;
    [SerializeField]
    private GameObject cameraPlayer;
    private Vector3 cameraPosition;
    [SerializeField]
    private Animator playerAnimator;
    [SerializeField]
    private float horizontalSpeed, verticalSpeed;
    [HideInInspector]
    public float horizontalActivation, verticalActivation;
    private float horizontalAcceleration = 5f, verticalAcceleration = 3.5f;//private Serializefield //public [HideInInspector]  
    [SerializeField]
    private Joystick joystick;



    private float score;
    private float countdown;
    [HideInInspector]
    public bool isTurning;

    private GameObject Player;
    [HideInInspector]
    public Vector3 rotationActivation;
    [SerializeField]
    private GameObject countdownGO, scoreUI, menuScript, nextlevelBtn, luckyWheelBtn, loseMenu, pauseBtn;


    [SerializeField]
    private GameObject[] scoreUpdate;

    [SerializeField]
    private ParticleSystem explosionPlayerParticle;


    private bool isCollidingShootZone, isCollidingFinish, isCollidingDie;
    [SerializeField]
    private SpaceShips_ScriptableObject spaceshipSO;




    [SerializeField]
    private GameObject adsManager;

    private float sensibility;
    private float Isensibility;

    [HideInInspector]
    public bool isAlive;
    public bool tutoMov;
    private bool isCounting;

    string prewtext;
    // Start is called before the first frame update
    void Start()
    {
        explosionPlayerParticle.gameObject.SetActive(false);
        cameraPosition = cameraPlayer.transform.localPosition;
        isAlive = true;
        GetModel(spaceshipSO.spaceships[spaceshipSO.index]);
        Player = transform.parent.gameObject;
        explosionPlayerParticle.Stop();
        score = 0;
        countdown = 3.4f;
        scoreUI.SetActive(false);
        loseMenu.SetActive(false);

        Isensibility = spaceshipSO.sensibility;
        if (Isensibility < 1) Isensibility = 1;
        sensibility = Isensibility * 0.3f;
        prewtext = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (!isCounting && playerAnimator.GetBool("Tuto") && PlayerPrefs.GetInt("passTuto") == 1 && !isCollidingFinish) {
            countdownGO.SetActive(true);
            playerAnimator.speed = 1f;
            Countdown();
        }
        Movement();

        foreach (GameObject scr in scoreUpdate) {
            scr.GetComponent<Text>().text = "Score: " + score.ToString();
        }
        if (isAlive) {
            transform.rotation = Quaternion.Euler(rotationActivation);
            Player.transform.position += new Vector3(horizontalActivation * sensibility, verticalActivation * sensibility, 0) * Time.deltaTime;
        }
    }

    public void Movement() {


        horizontalActivation = Mathf.Lerp((horizontalActivation), joystick.Horizontal * horizontalSpeed, horizontalAcceleration * Time.deltaTime);
        rotationActivation = new Vector3(joystick.Vertical * -8f, 0, joystick.Horizontal * -10f);
        verticalActivation = Mathf.Lerp(verticalActivation, joystick.Vertical * verticalSpeed, verticalAcceleration * Time.deltaTime);


        if (playerAnimator.GetBool("Tuto") && !isCollidingFinish)
        {
            if (spaceshipSO.currentLevel < 5)
            {
                playerAnimator.speed = 0.96f + (1.55f * (spaceshipSO.currentLevel / 100f));
            }
            else
            {
                playerAnimator.speed = 0.96f + (1.6f * (spaceshipSO.currentLevel / 95f));
            }
        }

    }



    private void Shoot()
    {
        GameObject bullet = BulletPool.SharedInstance.bulletPool();
        if (bullet != null)
        {
            bullet.transform.position = transform.position + (Vector3.forward * 0.5f);
            bullet.SetActive(true);
            bullet.GetComponent<BulletShot>().isExploded = false;
            bullet.GetComponent<BulletShot>().playerPosition = transform.position;
            bullet.GetComponent<BulletShot>().bulletSfx.source.Play();

        }
    }

    public void ScoreRegister(int addScore) {
        score += addScore;

    }
    private void Countdown() {

        countdown -= Time.deltaTime;


        countdownGO.GetComponentInChildren<TMP_Text>().text = countdown.ToString("f0");


        if (countdown > 0) {
            if (countdownGO.GetComponentInChildren<TMP_Text>().text != prewtext && countdown > 1) FindObjectOfType<AudioManager>().PlaySound("SoundCountdown");
            if (countdownGO.GetComponentInChildren<TMP_Text>().text != prewtext && countdown < 1) FindObjectOfType<AudioManager>().PlaySound("SoundCountdown2");

        }

        prewtext = countdownGO.GetComponentInChildren<TMP_Text>().text;
        if (countdown < 0.5 && countdown > 0) {

            FindObjectOfType<AudioManager>().PlaySound("MusicMenuLevels");
            countdownGO.GetComponentInChildren<TMP_Text>().text = "Start";
        }
        if (countdown < 0 && countdown > -1)
        {
            countdownGO.SetActive(false);
            pauseBtn.SetActive(true);
            playerAnimator.SetBool("StartAnim", true);
            isCounting = true;
            SpaceShipTail.SetActive(true);

        }
    }

    public void GetModel(Spaceships model) {

        if (model.spaceship.GetComponent<Renderer>() == null)
        {
            Renderer[] spaceshipsRenderer = model.spaceship.GetComponentsInChildren<Renderer>();
            foreach (Renderer renderer in spaceshipsRenderer)
            {
                if(!renderer.gameObject.CompareTag("Particle"))renderer.material = spaceshipSO.materials[model.activeMaterialIndex].material;
            }

        }
        else
        {
            model.spaceship.GetComponent<Renderer>().material = spaceshipSO.materials[model.activeMaterialIndex].material;
        }

        SpaceshipModel = Instantiate(model.spaceship, this.transform.position, Quaternion.identity);
        SpaceshipModel.SetActive(true);
        SpaceshipModel.transform.parent = gameObject.transform;
        SpaceshipModel.transform.localScale = Vector3.one;
        SpaceShipTail = SpaceshipModel.transform.GetChild(0).gameObject;
        SpaceShipTail.SetActive(false);
    }
    public void CameraShake(float duration, float magnitude) {
        StartCoroutine(CameShake(duration, magnitude));

    }
    private void WheelAnimation () {
        Transform[] childern;
        childern = luckyWheelBtn.GetComponentsInChildren<Transform>();
        PickerWheel wheel = childern[1].GetComponent<PickerWheel>();
        wheel.spinDuration = 1000;
        //wheel.Spin();
    }
    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Scene") || col.CompareTag("Enemies") && !isCollidingDie)
        {
            isCollidingDie = true;
            isAlive = false;

            FindObjectOfType<AudioManager>().PlaySound("SfxExplosion");
            pauseBtn.SetActive(false);          
            SpaceshipModel.SetActive(false);   
            explosionPlayerParticle.gameObject.SetActive(true);
            explosionPlayerParticle.Play();
            playerAnimator.SetBool("Tuto",false);
            playerAnimator.speed = 0.01f;
            StartCoroutine("Lose");
            FindObjectOfType<GAManager>().lose();
            CameraShake(0.3f, 1.2f);
        }
        if (col.CompareTag("Finish") && !isCollidingFinish){
            pauseBtn.SetActive(false);
            StartCoroutine(WaitFor(0.5f));
            isCollidingFinish = true;
            scoreUpdate[0].SetActive(false);   
            scoreUI.SetActive(true);
            scoreUI.GetComponent<Score>().ScoreStar(score/1000);
            FindObjectOfType<AudioManager>().StopAllSound();
            playerAnimator.speed = 0.001f;
            joystick.gameObject.SetActive(false);
            //
            if (score > 999)
            {
                nextlevelBtn.SetActive(true);
                luckyWheelBtn.SetActive(true);
                WheelAnimation();
                menuScript.GetComponent<Menu>().PassLevel(true);
                FindObjectOfType<GAManager>().levelComplete();
                PlayerPrefs.SetInt("canReward", 1);
            }
            else {
                luckyWheelBtn.SetActive(false);
                nextlevelBtn.SetActive(false);
            }
            
            
        }
        if (col.CompareTag("ShootZone") && !isCollidingShootZone) {
            isCollidingShootZone = true;
            StartCoroutine("ShootAutomatic");
        }
    }
    private void OnTriggerExit(Collider col) 
    {
        if (col.CompareTag("ShootZone") && isCollidingShootZone)
        {
            isCollidingShootZone = false;
            StopCoroutine("ShootAutomatic");
        }
            
    }



     private IEnumerator Lose() 
    {
        yield return new WaitForSeconds(0.5f);
        loseMenu.SetActive(true);
        FindObjectOfType<AudioManager>().StopAllSound();
        yield return new WaitForSeconds(0.5f);
        if(menuScript.GetComponent<Menu>().isAds)adsManager.GetComponent<AdsManager>().ShowAd("Interstitial");
        Time.timeScale = 0;
    }
    private  IEnumerator ShootAutomatic()
    {
        Shoot();
        yield return new WaitForSeconds(0.25f);      
        StartCoroutine("ShootAutomatic");
    }

    private IEnumerator WaitFor(float time) {
        yield return new WaitForSeconds(time);
        if(spaceshipSO.currentLevel % 3 == 0)adsManager.GetComponent<AdsManager>().ShowAd("Interstitial");
        gameObject.SetActive(false);
        //Time.timeScale = 0;
    }

    private IEnumerator CameShake(float duration, float magnitude ) {
        float shakeTime = 0.0f;
  
        while (shakeTime< duration) {
            float posx = Random.Range(-1, 1) * magnitude;
            float posy = Random.Range(-1, 1) * magnitude + cameraPosition.y;
            cameraPlayer.transform.localPosition = new Vector3(posx,posy, cameraPosition.z);
            shakeTime += Time.deltaTime;
            yield return null;
        }

        cameraPlayer.transform.localPosition = cameraPosition;
        
    }

}
