using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SpaceShips_ScriptableObject : ScriptableObject
{
    [SerializeField]
    public Spaceships[] spaceships;
    [SerializeField]
    public MaterialsSpaceship[] materials;
    [SerializeField]
    public int index;
    [SerializeField]
    public List<Level> levels;
    [SerializeField]
    public int currentLevel;
    [SerializeField]
    public float sensibility;
    [SerializeField]
    public int money;
}
