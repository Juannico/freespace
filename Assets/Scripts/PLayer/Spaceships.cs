
using UnityEngine;

[System.Serializable]
public class Spaceships
{
    public GameObject spaceship;
    public string name;
    public bool isPlayable;
    public int price;
    [SerializeField]
    public int activeMaterialIndex;

}

