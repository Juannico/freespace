using System.Collections;
using System;
using UnityEngine;

public class BulletShot : MonoBehaviour
{
    [SerializeField]
    private float bulletVelocity;
    private float velocity, velocityInverse;

    private Vector3 explosionPosition;
    [SerializeField]
    private GameObject explosion, bulletLineRenderer;
    public bool isExploded;
    // Start is called before the first frame update
    [HideInInspector]
    public Vector3 playerPosition;
    [HideInInspector]
    public Sounds bulletSfx;
    private Sounds explosionSfx;

    void Awake()
    {
        velocity = bulletVelocity;
        velocityInverse = 0;
        explosion.SetActive(false);
        explosionPosition = explosion.transform.position;
        bulletSfx = Array.Find(FindObjectOfType<AudioManager>().sounds, sound => sound.name == "SfxBullet");
        explosionSfx = Array.Find(FindObjectOfType<AudioManager>().sounds, sound => sound.name == "SfxExplosion");
        explosionSfx.source.pitch = 2;
        explosionSfx.source.volume = 0.05f;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        this.GetComponent<Rigidbody>().velocity = Vector3.forward * velocity;
        explosion.transform.localPosition += Vector3.back * velocityInverse * Time.deltaTime;
        if (transform.position.z > playerPosition.z + 110 && !isExploded) {
             this.gameObject.SetActive(false);
            isExploded = true;
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.layer != 6 && !isExploded) {
            explosionSfx.source.Play();
            isExploded = true;
            explosion.SetActive(true);
            explosion.transform.localPosition = explosionPosition;        
            bulletLineRenderer.SetActive(false);
            StartCoroutine("BulletExplosion");
            velocityInverse = bulletVelocity;
           
        }
       


    }
    private IEnumerator BulletExplosion() {
        
        yield return new WaitForSeconds(1f);      
        explosion.SetActive(false);
        bulletLineRenderer.SetActive(true);
        this.gameObject.SetActive(false);
        
        velocityInverse = 0;
        

    }


}
