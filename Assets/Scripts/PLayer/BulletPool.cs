
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{

    public static BulletPool SharedInstance;
    [SerializeField]
    private int amountBullet;
    [SerializeField]
    private GameObject bulletI;
    private List<GameObject> bulletsPool = new List<GameObject>();
    [SerializeField]
    private GameObject bullets;

    // Start is called before the first frame update

    private void Awake()
    {
        if (SharedInstance == null)
        {
            SharedInstance = this;
        }
    }
    void Start()
    {
        GameObject temp;
        for (int i = 0; i < amountBullet; i++)
        {
            temp = Instantiate(bulletI);
            temp.SetActive(false);
            temp.transform.parent = bullets.transform;
            temp.transform.parent = bullets.transform;
            bulletsPool.Add(temp);
        }
       

    }

    public GameObject bulletPool()
    {

        for (int i = 0; i < amountBullet; i++)
        {
            if (!bulletsPool[i].activeInHierarchy)
            {
                return bulletsPool[i];
            }
        }
        return null;
    }
}

