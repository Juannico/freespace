
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public bool[] pass = new bool[5];
    [HideInInspector]
    public int currentLv;
    public GameObject pause;
    public GameObject alert;
    private int countSameScene;
    [HideInInspector]
    public bool isAds;

    public GameObject score;

    private Scene scene;
    [SerializeField]
    private GameObject  adsManager, luckyWheel, luckyWheelBtn, pauseBtn;


    [SerializeField]
    private LevelsIcon[] levelIcon;
    [SerializeField]
    public Scrollbar movementSpaceship;
    [SerializeField]
    private SpaceShips_ScriptableObject spaceshipSO;

    [SerializeField]
    private TMP_Text levelTittle;

    private int prewIndexScene;

    // Start is called before the first frame update
    void Start()
    {
        countSameScene = PlayerPrefs.GetInt("countSameScene");
        scene = SceneManager.GetActiveScene();
         prewIndexScene = PlayerPrefs.GetInt("prewSceneIndex");
       
        if (scene.name == "MenuStart") {
            spaceshipSO.currentLevel = -1;         
                while (spaceshipSO.levels.Count > 5) {
                spaceshipSO.levels.RemoveAt(spaceshipSO.levels.Count - 1);
                }
            for (int i = 0; i < spaceshipSO.levels.Count; i++)
            {
                if (i <= spaceshipSO.currentLevel - 1)
                {
                    spaceshipSO.levels[i].pass = true;
                }
                else
                {
                    spaceshipSO.levels[i].pass = false;
                }
            }
        }
        spaceshipSO.levels[0].pass = true;

        if (spaceshipSO.currentLevel > -1 && scene.name != "Settings") {
            levelTittle.text = "level " + (spaceshipSO.currentLevel + 1);       
            if (scene.buildIndex == PlayerPrefs.GetInt("prewSceneIndex") && countSameScene < 6)
            {
                countSameScene++;
                PlayerPrefs.SetInt("countSameScene", countSameScene);
               
            }
            else
            {
                countSameScene = 0;
                PlayerPrefs.SetInt("countSameScene", 0);
                isAds = false;
            }
        }
        if ((spaceshipSO.currentLevel + 1) % 5 == 0  && countSameScene == 0  && scene.buildIndex < 6 && scene.buildIndex > 0)
        {
            CreateRandomLevels();
        }
        if (countSameScene == 5)
        {
            isAds = true;
        }
        // if (scene.name == "MenuLevels") SetLevels();
        if (scene.name == "Settings") movementSpaceship.value = spaceshipSO.sensibility - 1;
        PlayerPrefs.SetInt("prewSceneIndex", scene.buildIndex); 
    }

    // Update is called once per frame
    void Update()
    {

       
    }

    public void MenuStart  ()
    {
        if (Time.timeScale == 0) Time.timeScale = 1;
        SceneManager.LoadScene("MenuStart");

    }
    public void MenuLevels() {
        if (Time.timeScale == 0) Time.timeScale = 1;
        SceneManager.LoadScene("MenuLevels");
       FindObjectOfType<AudioManager>().StopAllSound();
 
        FindObjectOfType<AudioManager>().PlaySound("MusicMenuLevels");
       
    }

    public void LoadLevel(int lv) {
        Time.timeScale = 1;
        spaceshipSO.currentLevel += lv;
        if (spaceshipSO.levels[spaceshipSO.currentLevel].pass)
        {
            SceneManager.LoadScene(spaceshipSO.levels[spaceshipSO.currentLevel].levelIndex);
            FindObjectOfType<AudioManager>().StopAllSound();
        }

    }
    public void ExitGame() 
    {
        if (Time.timeScale == 0) Time.timeScale = 1;
        Application.Quit();
    }                   

    public void PauseGame() {
        pause.SetActive(true);
        FindObjectOfType<AudioManager>().PauseAllSound();
        Time.timeScale = 0;
        pauseBtn.SetActive(false);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        pause.SetActive(false);
        FindObjectOfType<AudioManager>().UnPauseAllSound();
        pauseBtn.SetActive(true);
    }


    public void PassLevel(bool passLv) {
        spaceshipSO.levels[spaceshipSO.currentLevel + 1 ].pass = true;
    }

    public void SelectSpaceshipScene() {
        if (Time.timeScale == 0) Time.timeScale = 1;
        SceneManager.LoadScene("SelectSpaceship"); 
    }
    public void SettingsScene() {
        if (Time.timeScale == 0) Time.timeScale = 1;
        SceneManager.LoadScene("Settings");
    }
    public void BackScene() {
        if (Time.timeScale == 0) Time.timeScale = 1;
        SceneManager.LoadScene(prewIndexScene);
    }

    public void ButtonSfx(string name) {
        
       FindObjectOfType<AudioManager>() .PlaySound(name);
    
    }

    private bool IntToBool(int passInt) {
        bool pass = false;
        if (passInt == 1) pass = true;
        return pass;
    }
  
    public void GetMoney(string adUnityId) {
        adsManager.GetComponent<AdsManager>().isRewarded = false;
        adsManager.GetComponent<AdsManager>().ShowAd(adUnityId);
    }
    public void LuckyWheel(bool isClose) {
        if (isClose)
        {
            Time.timeScale = 1;
            luckyWheelBtn.SetActive(false);
            luckyWheel.SetActive(true);
        }
        if(!isClose)
        {
            Time.timeScale = 0;
            luckyWheel.SetActive(false);
        }
    }

    public void SetSensibility() {
        spaceshipSO.sensibility = movementSpaceship.value + 1;
    }

    public void LoadCredits() {
        SceneManager.LoadScene("Credits");
    }
    /*
    private void SetLevels()
    {
        for (int i = 0; i < pass.Length; i++)
        {
            if (pass[i])
            {
                levelIcon[i].LevelBtn.GetComponent<Image>().color = levelIcon[i].activeColor;
                levelIcon[i].levelText.SetActive(true);
                levelIcon[i].iconLock.SetActive(false);
            }
            else {
                levelIcon[i].LevelBtn.GetComponent<Image>().color = levelIcon[i].lockColor;
                levelIcon[i].levelText.SetActive(false);
                levelIcon[i].iconLock.SetActive(true);
            }
        }
    }
    */
    private void CreateRandomLevels() {
        List<int> levels = new List<int>();
        for (int i = 1; i < 6; i++)
        {
            levels.Add(i);
        }
        for (int i = 0; i < levels.Count ; i++)
        {
            int randomIndex = Random.Range(0, levels.Count - 1 );
            int levelToSwap = levels[randomIndex];
            levels[randomIndex] = levels[i];
            levels[i] = levelToSwap;
        }
        if (levels[0] == spaceshipSO.levels[spaceshipSO.currentLevel].levelIndex) {
            int randomIndex = Random.Range(1, levels.Count - 1);
            int levelToSwap = levels[randomIndex];
            levels[randomIndex] = levels[0];
            levels[0] = levelToSwap;
        }
        
        foreach (int levelIndex in levels) {
            spaceshipSO.levels.Add(new Level());
            spaceshipSO.levels[spaceshipSO.levels.Count - 1].levelIndex = levelIndex;
        }
    }
    private IEnumerator IconShake(int i) {
        float duration = 0.5f;
        float shakeTime = 0;
        float posx = 1f;
        while (shakeTime < duration) {
            posx *= -Random.Range(0.9f,1.1f);
            levelIcon[i].iconLock.transform.position += new Vector3(posx,0,0);
            shakeTime += Time.deltaTime;
            yield return null;
        }
        levelIcon[i].iconLock.transform.localPosition = Vector3.zero;
    }
}
