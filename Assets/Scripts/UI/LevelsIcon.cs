using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class LevelsIcon 
{
    // Start is called before the first frame update
    public GameObject LevelBtn;
    public GameObject levelText;
    public GameObject iconLock;
    
    public Color activeColor = new Color(1,1,1);
    public Color lockColor = new Color(0.2f, 0.2f, 0.2f);
}
