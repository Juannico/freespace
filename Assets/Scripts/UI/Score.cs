
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text scoreTx;
    public Text phraseTx;
    public GameObject[] stars;
    private string[] phrase = new string[3] { "Good" , " Excelent", " Perfect"}; 
    private float score;
    // Start is called before the first frame update


   public void ScoreStar(float scorePercentage) {
        score = scorePercentage * 1000;
        scoreTx.text = "Score: " + score.ToString();
        for (int i = 0; i<stars.Length; i++) {
           
            if (scorePercentage - i >= 0 ) {
                stars[i].transform.localScale = new Vector3(1,scorePercentage - i ,1); 
            }
        }
    }

}
