﻿using UnityEditor;

public class CreateBundles{
    #if UNITY_EDITOR
    [MenuItem("Assets/Build AssetBundles/All")]
    static void BuildAllAssetBundles(){
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/and", BuildAssetBundleOptions.None, BuildTarget.Android);
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/ios", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }
    [MenuItem("Assets/Build AssetBundles/Android")]
    static void BuildAndroidAssetBundles(){
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/and", BuildAssetBundleOptions.None, BuildTarget.Android);
    }
    [MenuItem("Assets/Build AssetBundles/iOS")]
    static void BuildIOSAssetBundles(){
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles/ios", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }
    #endif
}