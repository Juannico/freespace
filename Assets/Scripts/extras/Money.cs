using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Money : MonoBehaviour
{
    [HideInInspector]
    public int money;
    [HideInInspector]
    private SpaceShips_ScriptableObject spaceshipSO;

    // Start is called before the first frame update
    void Start()
    {
        // money = 100;
        spaceshipSO = Resources.Load<SpaceShips_ScriptableObject>("SpaceShips");
    }

    // Update is called once per frame
    void Update()
    {
        //money = PlayerPrefs.GetInt("money");
        money = spaceshipSO.money;
        this.GetComponent<Text>().text = "Money: $" + money.ToString();
    }
    public void AddMoney(int value) {
        money += value;
        spaceshipSO.money = money;
        //PlayerPrefs.SetInt("money", money);
    }
    public void SubstactMoney(int value) {
        money -= value;
        spaceshipSO.money = money;
    }
}
