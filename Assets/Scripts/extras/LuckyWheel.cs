using UnityEngine.UI;
using UnityEngine;
using EasyUI.PickerWheelUI;
using TMPro;

public class LuckyWheel : MonoBehaviour
{
    [SerializeField] 
    private Button spinButton;
    [SerializeField]
    private TMP_Text spinButtonText;
    [SerializeField]
    private PickerWheel luckyWheel;
    [SerializeField]
    private GameObject money;

    private void Start()
    {
        spinButton.gameObject.SetActive(true);
   
    }
    public void StartSpining() {

        spinButton.interactable = false;
        spinButtonText.text = "Spinning";
        luckyWheel.OnSpinEnd(wheelPiece =>
        {
            money.GetComponent<Money>().AddMoney(wheelPiece.Amount);
            FindObjectOfType<AudioManager>().PlaySound("SfxCollect");
            // spinButton.gameObject.SetActive(false);
            //spinButtonText.text = "Spin";
        });
        luckyWheel.Spin();

    }
}
