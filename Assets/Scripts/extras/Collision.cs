
using System;
using UnityEngine;

public class Collision : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private int giveScore;

    bool isColliding = false;
    [SerializeField]
    private bool isEnemy ;

    [SerializeField]

    private ParticleSystem explosionSelf;

    private Sounds explosionSfx,collectSfx;
    // Start is called before the first frame update
    void Start()
    {
        explosionSelf.Stop();
    }


    // Update is called once per frame
    private void Update()
    {
        /*
        if (isColliding)
        {
            isColliding = false;
            
          //  this.GetComponent<BoxCollider>().enabled = false;
           // this.gameObject.SetActive(false);
        }
        */
    }
    private void OnTriggerEnter(Collider col)
    {
        if (!isColliding)
        {
            
            if (col.CompareTag("Bullet") && isEnemy)
            {

                explosionSelf.transform.position = this.transform.position;
                explosionSelf.Play();
                FindObjectOfType<AudioManager>().PlaySound("SfxExplosion");
                player.GetComponent<ShipController>().CameraShake(0.5f, 0.2f);         
            }
            if (col.CompareTag("Player") && !isEnemy)
            {
                explosionSelf.transform.position = this.transform.position + Vector3.forward * 2;
                explosionSelf.Play();
                FindObjectOfType<AudioManager>().PlaySound("SfxCollect");
            }
            if (col.CompareTag("Bullet") && !isEnemy)
            {
                giveScore = 0;
                explosionSelf.transform.position = this.transform.position;
                explosionSelf.Play();
                FindObjectOfType<AudioManager>().PlaySound("SfxExplosion");
            }
            if (!col.CompareTag("Untagged"))
            {
                isColliding = true;
                player.GetComponent<ShipController>().ScoreRegister(giveScore);
                this.GetComponent<MeshRenderer>().enabled = false;
                this.GetComponent<BoxCollider>().enabled = false;
            }

        }
    }
}
