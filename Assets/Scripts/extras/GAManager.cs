using UnityEngine;
using GameAnalyticsSDK;
using UnityEngine.SceneManagement;

public class GAManager : MonoBehaviour
{
    public static GAManager instance;
    private ShipController shipController;
    private Scene scene;

    private void Awake()
    {
        scene = SceneManager.GetActiveScene();
        instance = this;
        DontDestroyOnLoad(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        if (scene.buildIndex > 0 || scene.buildIndex < 6) {
            GameAnalytics.Initialize();          
            shipController = GameObject.Find("Spaceship").GetComponent<ShipController>();
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "LevelStart " + scene.buildIndex);
        }
    }

    // Update is called once per frame
    public void levelComplete() {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "levelComplete " + scene.buildIndex);
    }
    public void lose() {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "LevelFail " + scene.buildIndex);
    }
}
