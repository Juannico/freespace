using System.Collections;
using UnityEngine;

public class EnemiesMovement : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private float maxX, minX, maxY, minY;
    [SerializeField]
    private float maxVelocityX, minVelocityX, maxVelocityY, minVelocityY;
    private Vector3 movement; 
    [SerializeField]
    private float maxTimeX, minTimeX, maxTimeY, minTimeY;


    private float distanceMaxX, distanceMinX, distanceMaxY, distanceMinY;

    private float enemySize;

    private bool isChangingX;
    private bool isChangingY;



    void Start()
    {
        StartCoroutine(Movement(RandomFunction(minVelocityX, maxVelocityX, Random.Range(-1.1f, 1)), Random.Range(minTimeX, maxTimeX), 0));
        StartCoroutine(Movement(RandomFunction(minVelocityY, maxVelocityY, Random.Range(-1.1f, 1)), Random.Range(minTimeY, maxTimeY), 1));
        enemySize = transform.localScale.x ;
    }

    // Update is called once per frame
    void Update()
    {
        GoToCollide();
        transform.position += movement * Time.deltaTime;
        transform.Rotate(0,5*movement.x*Time.deltaTime,0);
        Deceleration();
    }
    private void FixedUpdate()
    {
        distanceMaxX = Mathf.Abs(maxX - this.transform.localPosition.x);
        distanceMinX = Mathf.Abs(minX - this.transform.localPosition.x);
        distanceMaxY = Mathf.Abs(maxY - this.transform.localPosition.y);
        distanceMinY = Mathf.Abs(minY - this.transform.localPosition.y);
    }
    private void Deceleration()
    {
        movement -= movement * 0.01f;
    }


    private void GoToCollide()
    {
        if (distanceMaxX < enemySize && !isChangingX)
        {
            isChangingX = true;
            StartCoroutine(MovementCorrection(RandomFunction(minVelocityX * 1.5f, maxVelocityX * 1.5f, -1f), Random.Range(minTimeX * 2.1f, maxTimeX * 1.2f), 0));
        }
        if (distanceMinX < enemySize && !isChangingX)
        {
            isChangingX = true;
            StartCoroutine(MovementCorrection(RandomFunction(minVelocityX * 1.5f, maxVelocityX * 1.5f, 1f), Random.Range(minTimeX * 2.1f, maxTimeX * 1.2f), 0));
        }

        if (distanceMaxY < enemySize && !isChangingY)
        {
            isChangingY = true;
            StartCoroutine(MovementCorrection(RandomFunction(minVelocityY * 1.4f, maxVelocityY * 1.4f, -1f), Random.Range(minTimeY * 2.1f, maxTimeY * 1.2f), 1));
        }
        if (distanceMinY < enemySize && !isChangingY)
        {
            isChangingY = true;
            StartCoroutine(MovementCorrection(RandomFunction(minVelocityY * 1.4f, maxVelocityY * 1.4f, 1f), Random.Range(minTimeY * 2.1f, maxTimeY * 1.2f), 1));
        }
    }
    private float RandomFunction(float min, float max, float side)
    {
        float randomNumeber;
        if (side >= 0)
        {
            randomNumeber = Random.Range(min, max);
        }
        else
        {
            randomNumeber = Random.Range(max * -1, min * -1);
        }
        return randomNumeber;
    }
    private IEnumerator Movement(float randomAxismovement, float randomTime, float axis)
    {
        if (axis == 0 && !isChangingX)
        {
            movement.x = randomAxismovement;
            yield return new WaitForSeconds(randomTime);
            StartCoroutine(Movement(RandomFunction(minVelocityX, maxVelocityX, Random.Range(-1.1f, 1)), Random.Range(minTimeX, maxTimeX), 0));
        }
        if (axis == 1 && !isChangingY)
        {
            movement.y = randomAxismovement;
            yield return new WaitForSeconds(randomTime * 0 + 2);
            StartCoroutine(Movement(RandomFunction(minVelocityY, maxVelocityY, Random.Range(-1.1f, 1)), Random.Range(minTimeY, maxTimeY), 1));

        }

    }
    private IEnumerator MovementCorrection(float randomAxismovement, float randomTime, float axis)
    {
        if (axis == 0)
        {
            movement.x = randomAxismovement;
            yield return new WaitForSeconds(randomTime);
            isChangingX = false;
            StartCoroutine(Movement(RandomFunction(minVelocityX, maxVelocityX, Random.Range(-1, 1)), Random.Range(minTimeX, maxTimeX), 0));
        }
        if (axis == 1)
        {
            movement.y = randomAxismovement;
            yield return new WaitForSeconds(randomTime * 0 + 2);
            isChangingY = false;
            StartCoroutine(Movement(RandomFunction(minVelocityY, maxVelocityY, Random.Range(-1.1f, 1)), Random.Range(minTimeY, maxTimeY), 1));
        }
    }

}
