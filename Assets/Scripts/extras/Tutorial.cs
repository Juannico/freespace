
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Tutorial : MonoBehaviour
{
    [SerializeField]
    private Animator playeraAnim;
    [SerializeField]
    private GameObject player, infoBackground, playerMov, controll;
    [SerializeField]
    private TMP_Text infoText;
    private int step;
    private bool pass;
    private Scene scene;
    private bool move;

    [SerializeField]
    private SpaceShips_ScriptableObject spaceshipSO;
    // Start is called before the first frame update

    private void Awake()
    {
        playeraAnim.SetBool("Tuto", false);
        scene = SceneManager.GetActiveScene();
        PlayerPrefs.SetInt("passTuto", 0);
        if (spaceshipSO.currentLevel == 0 )   
        {   
            //FindObjectOfType<AudioManager>().PlaySound("SoundSpaceShip");
            
            infoBackground.SetActive(true);
            playeraAnim.speed = 1;
        }
        else
        {
            playeraAnim.speed = 50f;
            gameObject.GetComponent<Tutorial>().enabled = false;
            PlayerPrefs.SetInt("passTuto", 1);
            playeraAnim.SetBool("Tuto", true);
            //  if (scene.buildIndex == 1) infoBackground.SetActive(false);

        }
    }
    private void Start()
    {
        
       
        
    }

    private void Update()
    {
        if (scene.buildIndex ==1 )  DontDie();
        switch (step)
        {
            case 0:
                if (player.GetComponent<ShipController>().horizontalActivation > 1) pass = true;
                break;
            case 1:
                if (player.GetComponent<ShipController>().horizontalActivation < -1) pass = true;
                break;
            case 2:
                if (player.GetComponent<ShipController>().verticalActivation > 1) pass = true;
                break;
            case 3:
                if (player.GetComponent<ShipController>().verticalActivation < -1) pass = true;
                break;
        }
        if (pass)
        {
            pass = false;
            step = 6;
            if (scene.buildIndex == 1 && PlayerPrefs.GetInt("passTuto") != 1)
            {
                playeraAnim.speed = 2;
               // infoBackground.SetActive(false);
            }
        }
    }
    public void PassTuto()
    {
        playeraAnim.SetBool("Tuto", true);
        PlayerPrefs.SetInt("passTuto", 1);
        if (scene.buildIndex == 1) infoBackground.SetActive(false);
        gameObject.GetComponent<Tutorial>().enabled = false;
    }
    public void SkipTuto()
    {
        playeraAnim.SetBool("Tuto", true);
        PlayerPrefs.SetInt("passTuto", 1);
        infoBackground.SetActive(false);
        playeraAnim.speed = 30f;
        gameObject.GetComponent<Tutorial>().enabled = false;
    }
    public void TutoRight()
    {
        SetStep(0, "Slide slowly and hold joystick rigth to move right");   
    }
    public void TutoLeft()
    {
        SetStep(1, "Slide slowly and hold joystick left to move left");
    }
    public void TutoUp()
    {
        SetStep(2, "Slide slowly and hold joystick up to move up");
    }
    public void TutoDown()
    {
        SetStep(3, "Slide slowly and hold joystick doiwn to move down");
    }
    public void EndTuto()
    {
        SetStep(6, "End of tutorial. Collect diamonds and kill enemies to get more than 1000 points to unlock next level");
    }
    private void SetStep(int currentlyStep, string infoTx)
    {
        if (spaceshipSO.currentLevel == 0 && PlayerPrefs.GetInt("passTuto") != 1)
        {
            infoBackground.SetActive(true);
            infoText.text = infoTx;
            step = currentlyStep;
            playeraAnim.speed = 0.02f;
                    if (step == 6)
                    {
                        playeraAnim.speed = 0.19f;
                    }
        }
    }

    private void DontDie()
    {
        if (playerMov.transform.localPosition.x > 58 && !move)
        {
            move = true;
            player.GetComponent<ShipController>().tutoMov = true;
            controll.SetActive(false);
            StartCoroutine(BackStart(1));
            playeraAnim.speed = 0;
        }
        if (playerMov.transform.localPosition.x < -58 && !move)
        {
            move = true;
            player.GetComponent<ShipController>().tutoMov = true;
            controll.SetActive(false);
            StartCoroutine(BackStart(2));
            playeraAnim.speed = 0;
        }
        if (playerMov.transform.localPosition.y > 37 && !move)
        {
            move = true;
            player.GetComponent<ShipController>().tutoMov = true;
            controll.SetActive(false);
            StartCoroutine(BackStart(3));
            playeraAnim.speed = 0;
        }
        if (playerMov.transform.localPosition.y < -18 && !move)
        {
            move = true;
            player.GetComponent<ShipController>().tutoMov = true;
            controll.SetActive(false);
            StartCoroutine(BackStart(4));
            playeraAnim.speed = 0;
        }
    }
    private IEnumerator BackStart(int side)
    {
        switch (side)
        {
            case 1:
                while (playerMov.transform.localPosition.x > 0)
                {
                    player.GetComponent<ShipController>().horizontalActivation = -17;
                    player.GetComponent<ShipController>().rotationActivation = Vector3.forward * 15;

                    yield return null;
                }
                break;
            case 2:
                while (playerMov.transform.localPosition.x < 0)
                {
                    player.GetComponent<ShipController>().horizontalActivation = 17;
                    player.GetComponent<ShipController>().rotationActivation = Vector3.back * 15;
                    yield return null;
                }

                break;

            case 3:
                while (playerMov.transform.localPosition.y > 0)
                {
                    player.GetComponent<ShipController>().verticalActivation = -14;
                    player.GetComponent<ShipController>().rotationActivation = Vector3.right * 14;
                    yield return null;
                }

                break;
            case 4:
                while (playerMov.transform.localPosition.y < 0)
                {
                    player.GetComponent<ShipController>().verticalActivation = 14;
                    player.GetComponent<ShipController>().rotationActivation = Vector3.left * 14;
                    yield return null;
                }
                break;

        }
        player.GetComponent<ShipController>().horizontalActivation = 0;
        player.GetComponent<ShipController>().verticalActivation = 0;
        player.GetComponent<ShipController>().rotationActivation = Vector3.zero;
        move = false;
        player.GetComponent<ShipController>().tutoMov = false;
        playeraAnim.speed = 1;
        controll.SetActive(true);
    }
    IEnumerator WaitFor(float second)
    {
        yield return new WaitForSeconds(second);

    }
}

