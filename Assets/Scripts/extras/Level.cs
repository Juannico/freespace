
using UnityEngine;

[System.Serializable]
public class Level 
{
    [SerializeField]
    public int levelIndex;
    [SerializeField]
    public bool pass;
    
}
