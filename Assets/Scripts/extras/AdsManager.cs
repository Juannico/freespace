
using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour,  IUnityAdsListener
{
    [SerializeField] 
    GameObject banner;
    [SerializeField] 
    string androidGameId;
    [SerializeField]
    string iOsGameId;
    [SerializeField] 
    bool testMode = false;
    private string gameId;
    private string Rewarded;
    [HideInInspector]
    public bool isRewarded;
    bool isAndroidplatform;

    // Start is called before the first frame update

    void Awake()
    {
       InitializeAds();
    }

     void Start()
    {
    }
    public void InitializeAds()
    {
        if (Application.platform == RuntimePlatform.Android) isAndroidplatform = true;
        if (isAndroidplatform) {
            gameId = androidGameId;
            Rewarded = "Rewarded_Android";
        }
        else {
            gameId = iOsGameId;
            Rewarded = "Rewarded_iOS";
        }
        Advertisement.Initialize(gameId, testMode);
        Advertisement.AddListener(this);
    }



    // Show the loaded content in the Ad Unit: 
    public void ShowAd(string adUnityId)
    {
        adUnityId = AdUniytId(adUnityId);
        Debug.Log("Loading Ad: " + adUnityId);
        Advertisement.Load(adUnityId);
 
        Debug.Log("Showing Ad: " + adUnityId);

            if (Advertisement.IsReady(adUnityId)) Advertisement.Show(adUnityId);
            else { StartCoroutine(starAd(adUnityId)); }

    
    }
    public void HideAdBanner(string adUnityId)
    {

        adUnityId = AdUniytId(adUnityId);
        Debug.Log("Hidding Ad: " + adUnityId);

            Advertisement.Banner.Hide(false);

    }



    private string AdUniytId(string unityId) {
        string currentId;
        if (isAndroidplatform)
        {
            currentId = unityId + "_Android";
        }
        else
        {
            currentId = unityId + "_iOS";
        }
        return currentId;
    }

    public void OnUnityAdsReady(string adUnityId)
    {
    }

    public void OnUnityAdsDidStart(string adUnityId)
    {
    }

    public void OnUnityAdsDidFinish(string adUnityId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished && adUnityId == Rewarded && !isRewarded) {
            isRewarded = true;
            FindObjectOfType<Money>().AddMoney(5);
            PlayerPrefs.SetInt("canReward",0);
        }
    }
    public void OnUnityAdsDidError(string adUnityId)
    {
        
    }

    private IEnumerator starAd(string adUnityId) {
        yield return new WaitForSeconds(0.5f);
        Advertisement.Show(adUnityId);
    }
}
